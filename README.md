![](./banner.png)


# Weighted Ranked Choice Voting

This Python script implements a weighted ranked choice voting system on the Dyson Protocol blockchain. It allows users to create and manage ballots, add and remove members with voting weights, and cast votes. It also calculates the winner based on the ranked choice voting algorithm.

## Features

- Create and update ballots with a title, description, start and end block, and a list of voting options.
- Set and remove members with voting weights.
- Cast votes with a ranked list of options.
- Calculate the winner based on the ranked choice voting algorithm.

## Functions

### create_ballot

Create a new ballot with the given title, description, start block, end block, and options. The start and end block can be absolute or relative values. The function returns the created ballot's ID.

```python
create_ballot(title: str, description: str, start_block: str, end_block: str, options: list[str]) -> int
```

### update_ballot

Update an existing ballot with the given title, description, start block, end block, and options. Only the admin who created the ballot can update it.

```python
update_ballot(ballot_id: int, title: str, description: str, start_block: int, end_block: int, options: list[str]) -> None
```

### set_member

Add a new member or update an existing member's voting weight for a specific ballot. Only the admin who created the ballot can set members.

```python
set_member(ballot_id: int, address: str, weight: int) -> None
```

### vote

Cast a vote with a ranked list of options for a specific ballot. The voter must be a member with a valid voting weight.

```python
vote(ballot_id: int, ranked_options: list[int]) -> None
```

### get_winner

Calculate the winner based on the ranked choice voting algorithm for a specific ballot.

```python
get_winner(ballot_id: int) -> str
```

## Usage Example

```python
# Create a new ballot
ballot_id = create_ballot(
    title="Best Fruit",
    description="Vote for your favorite fruit.",
    start_block="+10",
    end_block="+100",
    options=["Apple", "Banana", "Cherry", "Date"]
)

# Set members with voting weights
set_member(ballot_id, <dys address 1>, 2)
set_member(ballot_id, <dys address 2>, 3)

# Cast votes (for example, in the members' scripts)
# use the wallet of dys address 1
vote(ballot_id, [2, 0, 1, 3]) 

# use the wallet of dys address 2
vote(ballot_id, [2, 1, 0, 3]) 

# Calculate the winner (after the end block)
winner = get_winner(ballot_id)
print(winner)  # Output: "Cherry"
```

## License

This project is licensed under the MIT License. See the [LICENSE](LICENSE) file for more information.