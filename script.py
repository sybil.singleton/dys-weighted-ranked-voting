from dys import _chain, BLOCK_INFO, SCRIPT_ADDRESS, CALLER
import json


def _get_next_ballot_id():
    index = f"{SCRIPT_ADDRESS}/next_ballot_id"
    res = _chain("dyson/QueryStorage", index=index)
    if res["error"]:
        next_id = 1
    else:
        next_id = int(res["result"]["storage"]["data"])
    result = _chain(
        "dyson/sendMsgUpdateStorage",
        creator=SCRIPT_ADDRESS,
        index=index,
        data=str(next_id + 1),
        force=True,
    )
    assert result["error"] is None, f"Error updating storage: {result['error']}"
    return next_id


def _get_ballot_index(ballot_id: int):
    return f"{SCRIPT_ADDRESS}/ballots/{int(ballot_id):015}"


def get_ballot(ballot_id: int):
    result = _chain("dyson/QueryStorage", index=_get_ballot_index(ballot_id))
    if result["error"]:
        return None
    return json.loads(result["result"]["storage"]["data"])


def create_ballot(
    title: str,
    description: str,
    start_block: str,
    end_block: str,
    options: list[str],
):
    current_block = BLOCK_INFO.height

    if not start_block:
        start_block = str(current_block)

    if start_block.startswith("+"):
        start_block = current_block + int(start_block[1:])

    if end_block.startswith("+"):
        end_block = current_block + int(end_block[1:])

    start_block = int(start_block)
    end_block = int(end_block)

    ballot_id = _get_next_ballot_id()
    ballot = {
        "ballot_id": ballot_id,
        "admin": CALLER,
        "title": title,
        "description": description,
        "options": options,
        "start_block": start_block,
        "end_block": end_block,
        "members": {},
        "tally_data": [0 for i in range(len(options))],
    }
    _store_ballot(ballot)
    return ballot_id


def update_ballot(
    ballot_id: int,
    title: str,
    description: str,
    start_block: int,
    end_block: int,
    options: list[str],
):
    ballot = get_ballot(ballot_id)
    current_block = BLOCK_INFO.height

    if not ballot:
        raise ValueError(f"Ballot with ID {ballot_id} does not exist.")
    if CALLER != ballot["admin"]:
        raise ValueError("Only the admin can update the ballot.")
    if current_block > ballot["start_block"]:
        raise ValueError("Updating the ballot is no longer allowed.")

    ballot["title"] = title or ballot["title"]
    ballot["description"] = description or ballot["description"]
    ballot["start_block"] = start_block or ballot["start_block"]
    ballot["end_block"] = end_block or ballot["end_block"]
    ballot["options"] = options or ballot["options"]

    _store_ballot(ballot)


def set_member(ballot_id: int, address: str, weight: int):
    ballot = get_ballot(ballot_id)
    if not ballot:
        raise ValueError(f"Ballot with ID {ballot_id} does not exist.")
    if CALLER != ballot["admin"]:
        raise ValueError("Only the admin can set members.")
    if weight == 0:
        if address in ballot["members"]:
            del ballot["members"][address]
        else:
            raise ValueError(f"Member {address} not found.")
    else:
        ballot["members"][address] = weight
    _store_ballot(ballot)


def get_vote_index(ballot_id: int, voter: str):
    return f"{SCRIPT_ADDRESS}/ballots/{int(ballot_id):015}/votes/{voter}"


def get_vote(ballot_id: int, voter: str):
    result = _chain("dyson/QueryStorage", index=get_vote_index(ballot_id, voter))
    if result["error"]:
        return None
    return json.loads(result["result"]["storage"]["data"])


def vote(ballot_id: int, ranked_options: list[int]):
    # Check if the ballot exists
    ballot = get_ballot(ballot_id)
    if not ballot:
        raise ValueError(f"Ballot with ID {ballot_id} does not exist.")

    # Check if voting is allowed based on the block height
    current_block = BLOCK_INFO.height
    if not (ballot["start_block"] <= current_block <= ballot["end_block"]):
        raise ValueError("Voting is not allowed at this time.")

    # Check if the caller is a member with a valid weight
    if CALLER not in ballot["members"]:
        raise ValueError("Caller is not an eligible voter.")
    weight = ballot["members"][CALLER]

    # Check if the ranked_options list is valid
    if not (
        len(ranked_options) == len(ballot["options"])
        and sorted(ranked_options) == list(range(len(ballot["options"])))
    ):
        raise ValueError("Invalid ranked options provided.")

    # Retrieve the previous vote, if it exists
    previous_vote = get_vote(ballot_id, CALLER)

    # Subtract the previous vote from the tally data
    if previous_vote:
        for rank, option in enumerate(previous_vote["vote"]):
            ballot["tally_data"][option] -= weight / (rank + 1)

    # Update the tally data with the new vote
    for rank, option in enumerate(ranked_options):
        ballot["tally_data"][option] += weight / (rank + 1)

    # Store the updated ballot
    _store_ballot(ballot)

    # Create a vote data structure
    vote_data = {
        "voter": CALLER,
        "ballot_id": ballot_id,
        "vote": ranked_options,
        "block_height": current_block,
        "weight": weight,
    }

    # Store the new vote
    result = _chain(
        "dyson/sendMsgUpdateStorage",
        creator=SCRIPT_ADDRESS,
        index=get_vote_index(vote_data["ballot_id"], vote_data["voter"]),
        data=json.dumps(vote_data),
        force=True,
    )
    assert result["error"] is None, f"Error updating storage: {result['error']}"


def get_winner(ballot_id: int):
    # Check if the ballot exists
    ballot = get_ballot(ballot_id)
    if not ballot:
        raise ValueError(f"Ballot with ID {ballot_id} does not exist.")

    winner_index = max(
        range(len(ballot["tally_data"])), key=lambda i: ballot["tally_data"][i]
    )
    return ballot["options"][winner_index]


def _store_ballot(ballot_data):
    result = _chain(
        "dyson/sendMsgUpdateStorage",
        creator=SCRIPT_ADDRESS,
        index=_get_ballot_index(ballot_data["ballot_id"]),
        data=json.dumps(ballot_data),
        force=True,
    )
    assert result["error"] is None, f"Error updating storage: {result['error']}"

